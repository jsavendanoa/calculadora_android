package com.example.calculadora;

//Importamos las clases
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//Inicio de la clase Main Activity
public class MainActivity extends AppCompatActivity {

    //Componentes graficos que nos ayuda a recibir y mostrar texto
    private EditText Txt_Valor1;  //Objeto relacionado el Txt del valor 1
    private EditText Txt_Valor2;  //Objeto relacionado el Txt del valor 2
    private TextView Tv_Resultado;  //Objeto relacionado el TextView del resultado

    //Inicio del metodo OnCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Desde la parte logica, se conecta los elementos graficos
        Txt_Valor1 = (EditText) findViewById(R.id.Txt_Valor1);
        Txt_Valor2 = (EditText) findViewById(R.id.Txt_Valor2);
        Tv_Resultado = (TextView) findViewById(R.id.Resultado);
    }

    //Metodo para la suma
    public void Sumar(View view){
        String valor1 = Txt_Valor1.getText().toString();
        String valor2 = Txt_Valor2.getText().toString();
            //Conversion de string(texto) a double(numero)
        double num1 = Double.parseDouble(valor1);
        double num2 = Double.parseDouble(valor2);
            //Hacer la suma
        double Suma = (num1 + num2);

        String resultado = String.valueOf(Suma); //Conversion del resultado a string
        Tv_Resultado.setText(resultado);
    }

    //Metodo para la resta
    public void Restar(View view){
        String valor1 = Txt_Valor1.getText().toString();
        String valor2 = Txt_Valor2.getText().toString();
        //Conversion de string(texto) a double(numero)
        double num1 = Double.parseDouble(valor1);
        double num2 = Double.parseDouble(valor2);
        //Hacer la resta
        double resta= (num1 - num2);

        String resultado = String.valueOf(resta); //Conversion del resultado a string
        Tv_Resultado.setText(resultado);
    }

    //Metodo para la multiplicacion
    public void Multiplicar(View view){
        String valor1 = Txt_Valor1.getText().toString();
        String valor2 = Txt_Valor2.getText().toString();
        //Conversion de string(texto) a double(numero)
        double num1 = Double.parseDouble(valor1);
        double num2 = Double.parseDouble(valor2);
        //Hacer la multiplicacion
        double multi= (num1 * num2);

        String resultado = String.valueOf(multi); //Conversion del resultado a string
        Tv_Resultado.setText(resultado);
    }

    //Metodo para la divicion
    public void Dividir(View view){
        String valor1 = Txt_Valor1.getText().toString();
        String valor2 = Txt_Valor2.getText().toString();
        //Conversion de string(texto) a double(numero)
        double num1 = Double.parseDouble(valor1);
        double num2 = Double.parseDouble(valor2);
        //Hacer la divicion
        double div= (num1 / num2);

        String resultado = String.valueOf(div); //Conversion del resultado a string
        Tv_Resultado.setText(resultado);
    }
}